package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by chiaravarini on 08/03/17.
 */
public abstract  class GeneratorTest {

    private final static int START = 0;
    private final static int STOP = 10;  //+1 per RangeGenerator
    private int numberInSequence = 0;
    private SequenceGenerator sequenceGenerator;


    @Before
    public void init(TestStrategy startegy){
        this.numberInSequence = startegy.getNumberInSequence();
    }

    @Test
    public void afterEndRangeGetOptionalEmpty(){
        for(int i = START; i<numberInSequence; i++) {
            sequenceGenerator.next();
        }
        assertFalse(sequenceGenerator.next().isPresent());
    }

    @Test
    public void resetBringsBackAtBeginning(){
        for (int i = START; i < STOP/2; i++){
            assertEquals(sequenceGenerator.allRemaining().size(), (numberInSequence-i));
            sequenceGenerator.next();
        }

        sequenceGenerator.reset();
        assertEquals(sequenceGenerator.allRemaining().size(), START);
    }

    @Test
    public void testIsOver() {
        for (int i = START; i < numberInSequence; i++) {
            sequenceGenerator.next();
            assertFalse(sequenceGenerator.isOver());
        }
        sequenceGenerator.next();
        assertTrue(sequenceGenerator.isOver());
    }

}
