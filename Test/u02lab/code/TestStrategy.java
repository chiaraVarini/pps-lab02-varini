package u02lab.code;

/**
 * Created by chiaravarini on 08/03/17.
 */
public interface TestStrategy {

    int getNumberInSequence();
}
