package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by chiaravarini on 07/03/17.
 */
public class RangeGeneratorTest {

    private final static int START = 0;
    private final static int STOP = 10;

    private SequenceGenerator rangeGenerator;

    @Before
    public void init(){
        this.rangeGenerator = new RangeGenerator(START, STOP);
    }

    @Test
    public void rightCreation(){
        for(int i = START; i<=STOP; i++) {
            assertEquals((int) rangeGenerator.next().get(), i);
        }
    }

    @Test
    public void afterEndRangeGetOptionalEmpty(){
        for(int i = START; i<=STOP; i++) {
            rangeGenerator.next();
        }
        assertFalse(rangeGenerator.next().isPresent());
    }

    @Test
    public void resetBringsBackAtBeginning(){
        for (int i = START; i < STOP/2; i++){
            rangeGenerator.next();
        }
        rangeGenerator.reset();
        assertEquals((int) rangeGenerator.next().get(), START);
    }

    @Test
    public void testIsOver(){
        for (int i = START; i < STOP; i++){
            rangeGenerator.next();
            assertFalse(rangeGenerator.isOver());
        }
        rangeGenerator.next();
        assertTrue(rangeGenerator.isOver());
    }

    @Test
    public void testAllRemaining(){
        List<Integer> remainingElelement = rangeGenerator.allRemaining();

        for (int i = START; i < STOP; i++){
            rangeGenerator.next();
            assertFalse(rangeGenerator.isOver());
            remainingElelement.remove(0);
        }
        assertEquals(remainingElelement, rangeGenerator.allRemaining());
    }
}