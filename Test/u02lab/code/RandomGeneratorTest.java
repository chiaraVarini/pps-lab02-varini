package u02lab.code;

import com.sun.org.apache.bcel.internal.generic.ARRAYLENGTH;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by chiaravarini on 07/03/17.
 */
public class RandomGeneratorTest {

    private final static int START = 0;
    private final static int SEQUENCES_LENGTH = 10;

    private SequenceGenerator randomGenerator;

    @Before
    public void init(){
        this.randomGenerator = new RandomGenerator(SEQUENCES_LENGTH);
    }

    @Test
    public void isArrayOfBits01(){
        randomGenerator.allRemaining().forEach(bit -> assertTrue(bit.equals(0) || bit.equals(1)));
    }

    @Test
    public void afterEndRangeGetOptionalEmpty(){
        for(int i = START; i<SEQUENCES_LENGTH; i++) {
            randomGenerator.next();
        }
        assertFalse(randomGenerator.next().isPresent());
    }

    @Test
    public void testIsOver(){

        for (int i = START; i < SEQUENCES_LENGTH-1; i++){
            randomGenerator.next();
            assertFalse(randomGenerator.isOver());
        }

        randomGenerator.next();
        assertTrue(randomGenerator.isOver());
        randomGenerator.next();
        assertTrue(randomGenerator.isOver());
    }

    @Test
    public void resetBringsBackAtBeginning(){
        for (int i = START; i < SEQUENCES_LENGTH/2; i++){
            assertEquals(randomGenerator.allRemaining().size(), (SEQUENCES_LENGTH-i));
            randomGenerator.next();
        }

        randomGenerator.reset();
        assertEquals(randomGenerator.allRemaining().size(), SEQUENCES_LENGTH);
    }

}