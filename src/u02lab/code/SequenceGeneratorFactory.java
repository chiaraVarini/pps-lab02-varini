package u02lab.code;

/**
 * Created by Chiara Varini on 08/03/17.
 */

public interface SequenceGeneratorFactory {

    /**
     * Create a RangeGenerator that produced elements from start to stop included
     * @param start
     * @param stop
     * @return RangeGenerator
     */
    SequenceGenerator createRangeGenerator(final int start, final int stop);

    /**
     * Create a RandomGenerator that produced "sequenceLength" 0 or 1
     * @param sequenceLength
     * @return RandomGenerator
     */
    SequenceGenerator createRandomGenerator(final int sequenceLength);

}
