package u02lab.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop has been produced, lead to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and gives false, at the end and gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private final static int FIRTS_POSITION = 0;

    private List<Optional<Integer>> allRange = new ArrayList<>();
    private List<Integer> rangeRemaining = new ArrayList<>();


    public RangeGenerator(int start, int stop){
        for(int i = start; i<=stop; i++){
            allRange.add(Optional.of(i));
            rangeRemaining.add(i);
        }
    }

    @Override
    public Optional<Integer> next() {

        return (this.rangeRemaining.size()>0)?
                Optional.of(this.rangeRemaining.remove(FIRTS_POSITION)) :
                Optional.empty();
    }

    @Override
    public void reset() {
        this.rangeRemaining.clear();
        this.allRange.forEach(optional -> this.allRemaining().add(optional.get()));

    }

    @Override
    public boolean isOver(){
        return this.rangeRemaining.size()==0;
    }

    @Override
    public List<Integer> allRemaining(){
        return new ArrayList<>(this.rangeRemaining);
    }

}
