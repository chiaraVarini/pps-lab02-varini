package u02lab.code;

/**
 * Created by chiaravarini on 08/03/17.
 */
public class SequenceGeneratorFactoryImpl implements SequenceGeneratorFactory {

    @Override
    public SequenceGenerator createRangeGenerator(int start, int stop) {
        return new RangeGenerator(start, stop);
    }

    @Override
    public SequenceGenerator createRandomGenerator(int sequenceLenght) {
        return new RandomGenerator(sequenceLenght);
    }
}
