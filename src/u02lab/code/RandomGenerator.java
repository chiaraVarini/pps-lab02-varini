package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private int index = 0;
    private int sequenceLenght = 0;

    public RandomGenerator(int sequenceLength){
       this.sequenceLenght = sequenceLength;
    }

    @Override
    public Optional<Integer> next() {
        this.index++;
        return (this.index< sequenceLenght)?
                Optional.of((int)Math.round(Math.random())) :
                Optional.empty();
    }

    @Override
    public void reset() {
        this.index = 0;
    }

    @Override
    public boolean isOver() {
        return this.index >= this.sequenceLenght;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> result = new ArrayList<>();
        for(int i = 0; i<(sequenceLenght -index); i++){
            result.add((int)Math.round(Math.random()));
        }
        return result ;
    }

}
